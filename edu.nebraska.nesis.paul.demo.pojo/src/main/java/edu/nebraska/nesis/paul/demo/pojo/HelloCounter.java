package edu.nebraska.nesis.paul.demo.pojo;

import java.io.InputStream;
import java.io.OutputStream;
import java.io.FileNotFoundException;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.Properties;

public class HelloCounter {	
	private Properties prop;
	private int count = 0;
	public boolean isPersisted = true;
	
	public HelloCounter(){						
		InputStream input;
		
		prop = new Properties();
		
			try {
				input = new FileInputStream("count");
				prop.load(input);
				input.close();
			} catch (FileNotFoundException e) {
				this.writeCount("0");
				// TODO Auto-generated catch block
				e.printStackTrace();
			} catch (IOException e) {
				// TODO Auto-generated catch block
				this.isPersisted = false;
				e.printStackTrace();
			} finally{
				String inCount = prop.getProperty("hello.count");
				count = Integer.parseInt(inCount);
			}
	}
	
	public int getCount(){		
		count++;
		
		this.writeCount(Integer.toString(count));		
				
		return count;
	}
	
	public void reset(){
		count = 0;
		writeCount("0");
	}
	
	private boolean writeCount(String outCount){
		try {
			OutputStream output = new FileOutputStream("count");			
			prop.setProperty("hello.count", outCount);
			prop.store(output, null);	
			this.isPersisted = true;
		} catch (IOException e) {
			// TODO Auto-generated catch block			
			e.printStackTrace();
			this.isPersisted = false;
			return false;
		}
		return true;
	}
}
