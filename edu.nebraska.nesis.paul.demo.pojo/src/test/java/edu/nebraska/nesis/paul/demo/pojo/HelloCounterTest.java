package edu.nebraska.nesis.paul.demo.pojo;

import edu.nebraska.nesis.paul.demo.pojo.HelloCounter;
import junit.framework.Assert;
import org.junit.Test;

public class HelloCounterTest{
	HelloCounter hc = new HelloCounter();
	
	@Test
	public void testHelloConstructor(){		
		Assert.assertTrue("Connected to persistent storage",hc.isPersisted);		
	}
	
	@Test 
	public void testHelloGetCount(){
		//Ensure starting with zero
		hc.reset();
		
		//Ping three times
		Assert.assertEquals("First hit",hc.getCount(),1);
		Assert.assertEquals("Second hit",hc.getCount(),2);
		Assert.assertEquals("Third hit",hc.getCount(),3);
		
		//Test that class thinks its writing
		Assert.assertTrue("Writing to persistent storage",hc.isPersisted);

		//Make a new counter
		hc = new HelloCounter();
		
		//Ping three more times should pick up where we left off
		Assert.assertEquals("Fourth hit",hc.getCount(),4);
		Assert.assertEquals("Fifth hit",hc.getCount(),5);
		Assert.assertEquals("Sixth hit",hc.getCount(),6);	
		
		//Reset
		hc.reset();
		
		//Ping three times count should start at 1
		Assert.assertEquals("Seventh hit",hc.getCount(),1);
		Assert.assertEquals("Eighth hit",hc.getCount(),2);
		Assert.assertEquals("Ninth hit",hc.getCount(),3);
	}	
}
